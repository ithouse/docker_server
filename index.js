var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);


var clients = {};
var _room = null;

var first_client_id_in_room = {};


var same_positon_checker = {};

var transactions_stack = {};

var last_version = "";
server.listen(3000, function(){
    console.log('listening on *:3003330');
});


var affected_positions = [];
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});


char_identifier = {

    char : "",
    real_offset : null,
    my_position : null,
    siteId : null,
    counter : null,
    transaction_json : null

};
class User {

    constructor(name, id, color){
        this.name = name;
        this.id = id;
        this.color = color;
    }

}

io.on('connection', function(socket) {

    /**
     * When user disconnects he is removed from list of clients.
     * After remove change is emitted to other users.
     * If there is no client left in room, list of room clients and stacks are also removed.
     */
    socket.on('disconnect', function () {
        for (var room in clients) {
            for (var id in clients[room]) {
                if (id == socket.id) {
                    delete clients[room][id];
                    io.to(room).emit('synchronize_users', clients[room]);}
            }
            if (clients[room].length < 1) {
                delete clients[room];
                delete transactions_stack[room];
            }
        }
        console.log("Disconnect user", socket.id);
    });

    /**
     * When user connects he is added to list of clients.
     * After adding, change is emitted to other users.
     * If there is no client in room, list of room clients and stacks are also created.
     */
    socket.on('create', function (room) {
        socket.join(room);
        io.to(room).emit('set_room', room);
        if (clients[room] === 'undefined' || !clients[room]) {
            console.log("Vytvaram ROOM", room);
            clients[room] = {};
            transactions_stack[room] = [];
        }
        console.log("Add user and apply transactions from stack::\n ", socket.id, "::\n", transactions_stack[room]);
        io.to(socket.id).emit('add_user', socket.id);
        transactions_stack[room].forEach(function (transaction) {
            socket.to(socket.id).emit('apply_transactions_client', transaction)
        });

    });

    /**
     * After saving of document tranaction_stack is cleared because
     * All users reloaded content according to state of client where save was clicked.
     */
    socket.on('delete_stack', function (room) {
        console.log("mazem-----", room);
        transactions_stack[room] = [];
        socket.to(room).emit('reload_page');
    });

    /*
    * Transaction is send from client that made change.
    * Tranasction is added to transactionStack for its room,
    * transactions from tranactions_stack are applied for new connected users.
    * */

    socket.on('apply_transactions_server', function (transaction,room) {
        if(typeof transactions_stack[room] === 'undefined'  || !transactions_stack[room]){
            transactions_stack[room] = [];}
      //  transactions_stack[room].push(transaction);
        if(typeof transaction.position_from_left !== 'undefined'){
        let position_identifier = transaction.position_from_left + "|" + transaction.position_from_right;

        // if(position_identifier in Object(same_positon_checker).keys())
        // {
        //     //TODO something
        //     if(transaction.do_insert && transaction.insert_positions.length === 1){
        //     let switch_tr = {'switch_positions' : true,
        //
        //     }}
        // }
        }
        socket.broadcast.to(room).emit('apply_transactions_client', transaction)

    }),
    socket.on('synchronize_users_server',function (new_client, room) {
        if(!clients[room] || clients[room] === 'undefined'){
            clients[room] = {};}
        console.log("Sync",room,new_client,clients[room]);
        clients[room][new_client.id] = new_client;
        io.to(room).emit('synchronize_users', clients[room]);
    });
    socket.on('cursor_position_server', function (position,title,id,color,room) {
        console.log("IDEm SerVER CLICK");
        socket.broadcast.to(room).emit('cursor_position_home',position, title,id, color); //toot ide vsetkym

    });


});








