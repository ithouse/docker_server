FROM node:carbon

WORKDIR /Users/martin.danek/_skola/docker_server/src


COPY package*.json ./
RUN npm install
COPY . .

EXPOSE 3000

CMD [ "npm", "start" ]